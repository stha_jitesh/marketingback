package marketingback

import marketing.Userregister
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

class UserregisterController {

    def index() {
        render "test"
    }
    def insertUpdateUsers(){
        Userregister user
        String id=params.id;
        if(id=="" || id==null){
            user=new Userregister()
        }
        else {
            user=Userregister.get(params.id)
        }
        user.username=params.username;
        user.password=params.password;
        user.email=params.email;
        user.phone=params.phone;
        user.address=params.address;
        if(user.save(flush: true, failOnError: true))
        {
            render "success";
        }
        else {
            render "failed";
        }
    }

    def deleteUser(){
        Userregister user=Userregister.get(params.id);
        user.visibility=false;
        if(user.save(flush: true, failOnError: true)){
            render "success";
        }
        else {
            render "failed";
        }
    }

    def getVisibleUser(){
        def user = Userregister.findAllByVisibility(true)
        JSONArray array = new JSONArray()
        for (Userregister u : user) {
            JSONObject inner = new JSONObject()
            inner.put("name", u.username)
            inner.put("name", u.password)
            inner.put("name", u.email)
            inner.put("name", u.phone)
            inner.put("name", u.address)
            array.add(inner);
        }
        JSONObject obj = new JSONObject()
        obj.put("data", array)
        render obj;
    }

        def getTotalUser(){
            Userregister user=Userregister.get(params.id)
            def u=user.createCriteria();
            def user1=u.list {
                projections {
                    count()
                }
            }
            def a=user1[0]
            println "a ====== $a"
            JSONObject object = new JSONObject();
            object.put("users", user1[0])
            render object
        }


}

