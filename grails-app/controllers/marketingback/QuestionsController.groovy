package marketingback

import marketing.Questions
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

class QuestionsController {

    def index() {
        render "hello";
    }
    def insertUpdateQuestion(){
        Questions questions;
        String id=params.id
        if(id==null || id.equals("")){
            questions = new Questions();
        }
        else{
            questions=Questions.get(id)
        }
        questions.questions= params.questions;
        questions.answer= params.answer;
        if(questions.save(flush:true,failonError: true)){
            render "success"
        }
        else{
            render "failed"
        }
    }

    def deleteQuestion(){
        Questions questions=Questions.get(params.id)
        questions.visibility=false
        if(questions.save(flush: true, failOnError: true)){
            render "success"
        }
        else {
            render "failed"
        }

    }
    def getQuestionVisible(){
        def questions = Questions.findAllByVisibility(true)
        JSONArray array = new JSONArray()
        for(Questions que :questions){
            JSONObject inner = new JSONObject()
            inner.put("questions",questions.questions)
            inner.put("answer",questions.answer)
            inner.put("id",questions.id)
            array.add(inner);
        }
        JSONObject obj = new JSONObject()
        obj.put("data",array)
        render obj;
    }
}


