package marketingback

import marketing.Category
import marketing.SubCategory
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

class SubCategoryController {

    def index() {}
    def insertUpdate(){
         SubCategory sc;
        Category c = Category.get(params.cId)
        String id=params.scId;
        if(id==null || id.equals("")){
            sc = new SubCategory();
        }
        else{
            sc=SubCategory.get(id)
        }
        sc.name= params.name;
        c.addToSubCategory(sc);
        JSONObject object = new JSONObject()
        if(sc.save(flush:true,failonError: true)){
            object.put("save", "success");
        }
        else{
            object.put("save", "failed");
        }
        render object;
    }
    def getVisible(){
        def result = SubCategory.createCriteria()
        def subCategory = result.list{
            order('id', "desc");
            eq("visibility", true)
        }
        JSONArray array = new JSONArray()
        for(SubCategory sc :subCategory){
            JSONObject inner = new JSONObject()
            inner.put("name",sc.name)
            inner.put("id",sc.id)
            inner.put("cId",sc.categoryId)
            array.add(inner);

        }
        JSONObject obj = new JSONObject()
        obj.put("data",array)
        render obj;
    }

    def deleteSubCategory(){
        SubCategory subCategory=SubCategory.get(params.id);
        subCategory.visibility=false;
        JSONObject obj = new JSONObject();
        if(subCategory.save(flush: true, failOnError: true)){
            obj.put("save", "success")
        }
        else {
            obj.put("save", "failed")
        }
        render obj;
    }
    def getSubCategoryCount(){
        SubCategory subCategory=SubCategory.get(params.id)
        def total=subCategory.createCriteria();
        def result=total.list {
            projections {
                count()
            }
        }
        def a=result[0];
        JSONObject object=new JSONObject();
        object.put("subcategory", a[0]);
        render object
    }
    def subCategorybyCategory(){
        def category = Category.get(params.id);
        def subCategory = SubCategory.findAllByVisibilityAndCategory(true,category);
        JSONArray array = new JSONArray()
        for(SubCategory c :subCategory){
            JSONObject inner = new JSONObject()
            inner.put("name",c.name)
            inner.put("id",c.id)
            array.add(inner);
        }
        JSONObject obj = new JSONObject()
        obj.put("data",array)
        render obj;
    }
}
