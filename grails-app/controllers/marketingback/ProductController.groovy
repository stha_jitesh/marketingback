package marketingback

import marketing.Product
import marketing.SubCategory
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

class ProductController {

    def index() {
        render "test"
    }
    def insertUpdateProduct(){
        Product p;
        String id=params.id;
        JSONObject object=new JSONObject()
        SubCategory sc = SubCategory.get(params.cid1);
        if(id=="" || id==null){
            p=new Product()
        }
        else {
            p=Product.get(id)
        }
        p.name=params.name;
        p.type=params.type;
        p.description=params.description;
        p.discount=Double.parseDouble(params.discount);
        p.brand=params.brand;
        p.price=Double.parseDouble(params.price);
        sc.addToProduct(p);
        if(p.save(flush: true, failOnError: true)){
            object.put("save","success")
        }
        else{
            object.put("save","failed")
        }
        render object;
    }

    def getVisible() {
        def p = Product.findAll()
        JSONArray array = new JSONArray()
        for (Product P : p) {
            JSONObject inner = new JSONObject()
            inner.put("name", P.name)
            inner.put("price", P.price)
            inner.put("discount", P.discount)
            inner.put("brand", p.brand);
            inner.put("type", p.type);
            inner.put("description", p.description);
            array.add(inner);

        }
        JSONObject obj = new JSONObject()
        obj.put("data", array)
        render obj;
    }
}
