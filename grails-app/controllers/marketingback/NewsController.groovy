package marketingback

import marketing.News
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

class NewsController {

    def index() {
        render "Test"
    }
    def insertUpdateNews(){
        News n;
        String id=params.id;
        if(id=="" || id==null){
            n=new News()
        }
        else {
            n=News.get(params.id)
        }
       n.heading=params.heading;
        n.description=params.description;
//        Date a = new Date();
//        println "a = $a"
//        n.date = new Date();
        JSONObject object=new JSONObject()
        if(n.save(flush: true, failOnError: true)){
            object.put("save","success")
        }
        else {
           object.put("save","failed")
        }
    }
    def deleteNews(){
        News n=News.get(params.id)
        n.visibility=false;
        JSONObject object=new JSONObject()
        if (n.save(flush: true, failOnError: true)){
            object.put("save", "success")
        }
            else{
            object.put("save", "failed")
        }
    }
    def getNewsVisibile(){
        News n=News.findAllByVisibility()
        JSONArray array=new JSONArray()
        for (News news : n) {
            JSONObject inner = new JSONObject()
            inner.put("heading", n.heading)
            inner.put("description", n.description)
            inner.put("date", n.date)
            array.add(inner);
        }
        JSONObject obj = new JSONObject()
        obj.put("data", array)
        render obj;
    }
    def getTotalNews(){
        News n=News.get(params.id)
        def news=n.createCriteria()
        def news1=news.list {
            projections{
                count()
        }
        }
        def a=news1[0]
        JSONObject object = new JSONObject();
        object.put("news", news1[0])
        render object
    }
}


