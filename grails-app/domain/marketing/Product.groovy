package marketing

class Product {
    String name;
    String type;
    Double discount;
    String description;
    String brand;
    Double price;
    boolean visibility = true;
    static belongsTo = [subCategory: SubCategory]

    static constraints = {
    }
}
