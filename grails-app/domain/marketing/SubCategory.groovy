package marketing

class SubCategory {
    String name;
    boolean visibility = true;
    static belongsTo = [category:Category];
    static hasMany = [product: Product]

    static constraints = {
    }
}
